const SETUP_GIT_URL = "https://git.tecosaur.net/tec/Startup.jl"

const STARTUP_MANAGED_MESSAGE =
    "# !! Managed by the Setup package !!"

const STARTUP_CONTENT = """
$STARTUP_MANAGED_MESSAGE
if VERSION < v"1.7"
    @warn "Setup disabled as Julia \$VERSION < 1.7 is unsupported"
else
    try
        using Setup
    catch e
        if e isa ArgumentError && isnothing(Base.find_package("Setup")) # Package not found in current path
            let pkg_id = Base.PkgId(Base.UUID("44cfe95a-1eb2-52ea-b672-e2afdf69b78f"), "Pkg")
                Pkg = Base.require(pkg_id)
                cproj = Base.current_project()
                try
                    Pkg.activate()
                    Pkg.develop(path=joinpath(@__DIR__, "Setup"))
                finally
                    if isnothing(cproj)
                        Pkg.activate()
                    else
                        Pkg.activate(cproj)
                    end
                end
            end
            using Setup
        else
            rethrow()
        end
    end

    Setup.ensureglobalpkg("Revise")
    @async @eval using Revise
end
"""

function _install_paths()
    config = joinpath(first(Base.DEPOT_PATH), "config")
    startup = joinpath(config, "startup.jl")
    setup = joinpath(config, "Setup")
    (; config, startup, setup)
end

function install()
    (; config, startup, setup) = _install_paths()
    isdir(config) || mkpath(config)
    if isfile(startup)
        if readline(startup) == STARTUP_MANAGED_MESSAGE
            isdir(setup) && update()
        else
            @info "Moving original startup file to $(startup).old"
            mv(startup, startup * ".old")
        end
    end
    write(startup, STARTUP_CONTENT)
    if isdir(setup) && !isdir(joinpath(setup, ".git"))
        @warn "Setup folder exists, but it is not a git repository. Re-creating..."
        rm(setup, recursive=true)
    end
    if !isdir(setup)
        success(`git clone $SETUP_GIT_URL $setup`) ||
            @error "Failed to clone $SETUP_GIT_URL"
    end
end

function uninstall()
    (; startup, setup) = _install_paths()
    isfile(startup) && readline(startup) != STARTUP_MANAGED_MESSAGE &&
        rm(startup)
    isfile(startup * ".old") && !isfile(startup) && mv(startup * ".old", startup)
    if isdir(joinpath(setup, ".git")) && !isempty(read(Cmd(`git status --porcelain=v1`, dir=setup)))
        @warn "Unstaged changes in $(setup)! Manually fix this and try again."
    elseif isdir(setup)
        rm(setup, recursive=true, force=true)
    end
end

function update()
    (; startup, setup) = _install_paths()
    if readline(startup) != STARTUP_MANAGED_MESSAGE
        @warn "startup.jl is not managed by Setup, re-installing"
        return install()
    else
        write(startup, STARTUP_CONTENT)
    end
    if !ispath(setup) || !isdir(joinpath(setup, ".git"))
        ispath(setup) || @warn "Setup missing, re-installing"
        return install()
    end
    if success(Cmd(`git pull origin master`, dir=setup))
        @info "Sucessfully updated Setup"
    else
        @error "Failed to update Setup"
    end
end
