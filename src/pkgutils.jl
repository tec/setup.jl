@static if VERSION <= v"1.10"
    const Pkg = let pkg_id = Base.PkgId(Base.UUID("44cfe95a-1eb2-52ea-b672-e2afdf69b78f"), "Pkg")
        @something(get(Base.loaded_modules, pkg_id, nothing),
                   Base.require(pkg_id))
    end
    lazypkg() = Pkg

    include("../ext/PkgUtils.jl")
    using .PkgUtils
else
    const Pkg = Ref{Module}()
    function lazypkg()
        if !isassigned(Pkg)
            pkg_id = Base.PkgId(Base.UUID("44cfe95a-1eb2-52ea-b672-e2afdf69b78f"), "Pkg")
            Pkg[] = @something(get(Base.loaded_modules, pkg_id, nothing),
                               Base.require(pkg_id))
        end
        Pkg[]
    end
end

function withproject(f::Function, proj::Union{String, Nothing})
    Pkg = lazypkg()
    oldproj = Base.current_project()
    try
        if isnothing(proj)
            Pkg.activate()
        else
            Pkg.activate(proj)
        end
        f()
    finally
        if isnothing(oldproj)
            Pkg.activate()
        else
            Pkg.activate(oldproj)
        end
    end
end

function ensureglobalpkg(pkg::String)
    isnothing(Base.find_package(pkg)) || return
    withproject(nothing) do
        @info "Installing $pkg"
        lazypkg().add(pkg)
    end
end
